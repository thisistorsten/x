﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("x")]
[assembly: AssemblyDescription("Simple script language")]
[assembly: AssemblyProduct("x - Simple script language")]
[assembly: AssemblyCopyright("(c) 2017 Torsten Klinger")]

[assembly: AssemblyVersion("0.1.*")]