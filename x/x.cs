﻿/*	
 * 	
 *  x - Simple script language
 *  
 *  Copyright (c) 2017 Torsten Klinger
 * 	E-Mail: torsten.klinger@googlemail.com
 * 	
 * 	x is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU Lesser General Public
 * 	License as published by the Free Software Foundation; either
 * 	version 2.1 of the License, or (at your option) any later version.
 *	
 * 	x is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * 	Lesser General Public License for more details.
 *	
 * 	You should have received a copy of the GNU Lesser General Public
 * 	License along with x if not, write to the Free Software
 * 	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * 	
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace x
{
	public static class x
	{
		public static void Main(string[] args)
		{
			try
			{
				if (args.Length == 0)
					return;

				di();

				var x = fo(args[0]);

				foreach (var y in x)
				{
					var m = y.Substring(0, 1);
					var c = y.Split('=')[0].Substring(1, y.Split('=')[0].Length - 1);
					var v = y.Split('=')[1];

					switch (m)
					{
						case "$":
							s.Add(new s { c = c, v = vs(v) });
							break;
						case "*":
							i.Add(new i { c = c, v = vi(v) });
							break;
						case ".":
							d.Add(new d { c = c, v = vd(v) });
							break;
						case ">":
							switch (c)
							{
								case "pr":
									pr(v);
									break;
								case "ad":
									ad(v);
									break;
								case "su":
									su(v);
									break;
								case "my":
                                    my(v);
									break;
								case "dv":
                                    dv(v);
									break;
								case "cl":
                                    cl(v);
									break;
								case "dw":
                                    dw(v);
									break;
								case "df":
                                    df(v);
									break;
								case "fr":
                                    fr(v);
									break;
								case "fw":
                                    fw(v);
									break;
								case "fd":
                                    fd(v);
									break;
								case "fc":
                                    fc(v);
									break;
								case "hl":
									hl();
									break;
								case "cd":
                                    cd(v);
									break;
								case "dd":
                                    dd(v);
									break;
								case "dc":
                                    dc(v);
									break;
								case "cs":
                                    cs(v);
									break;
								case "ci":
                                    ci(v);
									break;
								case "cu":
                                    cu(v);
									break;
								case "dt":
                                    dt(v);
									break;
								case "wa":
                                    wa(v);
									break;
								case "sp":
                                    sp(v);
									break;
								case "ex":
                                    ex(v);
									break;
								case "sr":
                                    sr(v);
									break;
								case "hs":
                                    hs(v);
									break;
								case "fb":
                                    fb(v);
									break;
								case "bs":
                                    bs(v);
									break;
								case "wh":
									var h = new Thread(() => wh(v));
									h.Start();
									break;
								case "ds":
                                    ds(v);
									break;
							}
							break;
					}
				}

				if (!COMPILER.FLAG)
					rl();
			}
			catch { pr((object)0); }
		}

		internal static void Second(string[] args)
		{
			try
			{
				if (args.Length == 0)
					return;

				foreach (var y in args)
				{
					var m = y.Substring(0, 1);
					var c = y.Split('=')[0].Substring(1, y.Split('=')[0].Length - 1);
					var v = y.Split('=')[1];

					switch (m)
					{
						case ">":
							switch (c)
							{
								case "pr":
									pr(v);
									break;
								case "ad":
									ad(v);
									break;
								case "su":
									su(v);
									break;
								case "my":
									my(v);
									break;
								case "dv":
									dv(v);
									break;
								case "cl":
									cl(v);
									break;
								case "dw":
									dw(v);
									break;
								case "df":
									df(v);
									break;
								case "fr":
                                    fr(v);
									break;
								case "fw":
                                    fw(v);
									break;
								case "fd":
                                    fd(v);
									break;
								case "fc":
                                    fc(v);
									break;
								case "hl":
                                    hl();
									break;
								case "cd":
                                    cd(v);
									break;
								case "dd":
                                    dd(v);
									break;
								case "dc":
                                    dc(v);
									break;
								case "cs":
                                    cs(v);
									break;
								case "ci":
									ci(v);
									break;
								case "cu":
									cu(v);
									break;
								case "dt":
                                    dt(v);
									break;
								case "wa":
                                    wa(v);
									break;
								case "sp":
                                    sp(v);
									break;
								case "ex":
                                    ex(v);
									break;
								case "sr":
                                    sr(v);
									break;
								case "hs":
                                    hs(v);
									break;
								case "fb":
                                    fb(v);
									break;
								case "bs":
                                    bs(v);
									break;
								case "wh":
									var h = new Thread(() => wh(v));
									h.Start();
									break;
								case "ds":
                                    ds(v);
									break;
							}
							break;
					}
				}
			}
			catch
			{
				return;
			}
		}

		internal static string[] fo(string f)
		{
			return File.ReadAllLines(f);
		}

		internal static void di()
		{
			s = new List<s>();
			i = new List<i>();
			d = new List<d>();
		}

		internal static void pr(object t)
		{
			if (COMPILER.FLAG)
				RETURN.VALUE = t;

			Console.WriteLine(t);
		}

		internal static void pr(string b)
		{
			var r = b.Split(',');

			var o = "";

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "$":
						o = o +  s.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "*":
						o = o +  i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						o = o + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "'":
						o = o + vs(t.Substring(1, t.Length - 1));
						break;
				}
			}

			pr((object)o);
		}

		internal static void ad(string b)
		{
			var r = b.Split(',');

			var o = 0;
			var q = (decimal)0.0;

			string l = "";

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "*":
						o = o + i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						q = q + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "%":
						l = t.Substring(1, t.Length -1);
						break;
				}
			}

			if (q == (decimal)0.0)
			{
				if (l != "")
				{
					i.Add(new i { c = l, v = o });
					pr((object)o);
				}
				else
                    pr((object)o);
			}
			else
			{
				if (l != "")
				{
					d.Add(new d { c = l, v = (o + q) });
                    pr((object)(o + q));
				}
				else
                    pr((object)(o + q));
			}
		}

		internal static void su(string b)
		{
			var r = b.Split(',');

			var o = 0;
			var q = (decimal)0.0;

			string l = "";

			bool e = true;

			bool n = true;

			bool g = false;

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "*":
						if (e)
						{
							o = o + i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							e = false;
							g = true;
						}
						else
							o = o - i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						if (n)
						{
							q = q + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							n = false;
						}
						else
							q = q - d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "%":
						l = t.Substring(1, t.Length - 1);
						break;
				}
			}

			if (q == (decimal)0.0)
			{
				if (l != "")
				{
					i.Add(new i { c = l, v = o });
					pr((object)o);
				}
				else
					pr((object)o);
			}
			else
			{
				if (l != "")
				{
					if (g)
					{
						d.Add(new d { c = l, v = (o - q) });
						pr((object)(o - q));
					}
					else
					{
						d.Add(new d { c = l, v = (q - o) });
                        pr((object)(q - o));
					}
				}
				else
				{
                    if (g)
					{
                        pr((object)(o - q));
					}
					else
					{
						pr((object)(q - o));
					}
				}

			}
		}

		internal static void my(string b)
		{
			var r = b.Split(',');

			var o = 0;
			var q = (decimal)0.0;

			string l = "";

			bool e = true;

			bool j = true;

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "*":
						if (e)
						{
							o = o + i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							e = false;
						}
						else if (!e && q == (decimal)0.0)
							o = o * i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						if (j)
						{
							q = q + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							j = false;
						}
						else if (!j && o == 0)
							q = q * d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "%":
						l = t.Substring(1, t.Length -1);
						break;
				}
			}

			if (l != "")
			{
				if (o == 0)
				{
                    pr((object)(q));
				}
				else if (q == (decimal)0.0)
				{
					pr((object)(o));
				}
				else
				{
					pr((object)(q * o));
				}
			}
			else
			{
				if (o == 0)
				{
					d.Add(new d { c = l, v = q });
					pr((object)(q));
				}
				else if (q == (decimal)0.0)
				{
					i.Add(new i { c = l, v = o });
					pr((object)(o));
				}
				else
				{
					d.Add(new d { c = l, v = (q * o) });
					pr((object)(q * o));
				}
			}
		}

		internal static void dv(string b)
		{
			var r = b.Split(',');

			var o = 0;
			var q = (decimal)0.0;

			string l = "";

			bool e = true;

			bool j = true;

			bool c = false;

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "*":
						if (e)
						{
							o = o + i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							e = false;
							c = true;
						}
						else if (!e && q == (decimal)0.0)
							o = o / i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						if (j)
						{
							q = q + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
							j = false;
						}
						else if (!j && o == 0)
							q = q / d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "%":
						l = t.Substring(1, t.Length - 1);
						break;
				}
			}

			if (l != "")
			{
				if (o == 0)
				{
					pr((object)(q));
				}
				else if (q == (decimal)0.0)
				{
					pr((object)(o));
				}
				else
				{
					if (c)
					{
						pr((object)(o / q));
					}
					else
					{
						pr((object)(q / o));
					}
				}
			}
			else
			{
				if (o == 0)
				{
					d.Add(new d { c = l, v = q });
					pr((object)(q));
				}
				else if (q == (decimal)0.0)
				{
					i.Add(new i { c = l, v = o });
					pr((object)(o));
				}
				else
				{
					if (c)
					{
						d.Add(new d { c = l, v = (q * o) });
						pr((object)(o / q));
					}
					else
					{
						d.Add(new d { c = l, v = (q* o) });
						pr((object)(q / o));
					}
				}
			}
		}

		internal static void cl(string b)
		{
			var f = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var x = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var t = s.Find(a => a.c == b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1)).v;
			var k = b.Split(',')[3].Substring(1, b.Split(',')[3].Length - 1);

			int p;
			int l;

			if (f.Contains(x) && f.Contains(t))
			{
				p = f.IndexOf(x, 0) + x.Length;
				l = f.IndexOf(t, p);

				s.Add(new s { c = k, v = f.Substring(p, l - p) });
				pr((object)(f.Substring(p, l - p)));
			}

            pr((object)0);
		}

		internal static void dw(string b)
		{
			WebClient w = new WebClient();

			var j = w.DownloadString(s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v);
			var k = b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1);

			s.Add(new s { c = k, v = j });
			pr((object)j);
		}

		internal static void df(string b)
		{
			WebClient w = new WebClient();

			w.DownloadFile(s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v,
			               s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v);

            pr((object)1);
		}

		internal static void fr(string b)
		{
			var f = i.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var x = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var t = i.Find(a => a.c == b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1)).v;
			var u = b.Split(',')[3];

			var w = false;

			if (x.Equals("--"))
			{
				for (int p = f; p == t; p++)
				{
					if (!w)
					{
						i.Add(new i { c = "§o", v = p });
						w = true;
					}
					else
					{
						foreach (var y in i.Where(c => c.c == "§o"))
						{
							y.v = p;
						}
					}

					Second(new string[] { u.Replace("~", ">").Replace("-", "=") });
				}
			}
			else if (x.Equals("<-"))
			{
				for (int p = f; p <= t; p++)
				{
					if (!w)
					{
						i.Add(new i { c = "§o", v = p });
						w = true;
					}
					else
					{
						foreach (var y in i.Where(c => c.c == "§o"))
						{
							y.v = p;
						}
					}

                    Second(new string[] { u.Replace("~", ">").Replace("-", "=") });
				}
			}
			else if (x.Equals(">-"))
			{
				for (int p = f; p >= t; p++)
				{
					if (!w)
					{
						i.Add(new i { c = "§o", v = p });
						w = true;
					}
					else
					{
						foreach (var y in i.Where(c => c.c == "§o"))
						{
							y.v = p;
						}
					}

                    Second(new string[] { u.Replace("~", ">").Replace("-", "=") });
				}
			}
			else if (x.Equals("<"))
			{
				for (int p = f; p < t; p++)
				{
					if (!w)
					{
						i.Add(new i { c = "§o", v = p });
						w = true;
					}
					else
					{
						foreach (var y in i.Where(c => c.c == "§o"))
						{
							y.v = p;
						}
					}

                    Second(new string[] { u.Replace("~", ">").Replace("-", "=") });
				}
			}
			else if (x.Equals(">"))
			{
				for (int p = f; p > t; p++)
				{
					if (!w)
					{
						i.Add(new i { c = "§o", v = p });
						w = true;
					}
					else
					{
						foreach (var y in i.Where(c => c.c == "§o"))
						{
							y.v = p;
						}
					}

                    Second(new string[] { u.Replace("~", ">").Replace("-", "=") });
				}
			}
		}

		internal static void fw(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			if (!File.Exists(x))
			{
				File.WriteAllText(x, y);
			}
			else
				File.AppendAllText(x, y);

            pr((object)1);
		}

		internal static void fd(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;

			if (File.Exists(x))
			{
				File.Delete(x);

                pr((object)1);
			}
			else
                pr((object)0);
		}

		internal static void fc(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			if (File.Exists(x))
			{
				File.Copy(x, y);

                pr((object)1);
			}
			else
                pr((object)0);
		}

		internal static void cd(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;

			if (!Directory.Exists(x))
			{
				Directory.CreateDirectory(x);

                pr((object)1);
			}
			else
                pr((object)0);
		}

		internal static void dd(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;

			if (Directory.Exists(x))
			{
				Directory.Delete(x, true);

                pr((object)1);;
			}
			else
                pr((object)1);
		}

		internal static void dc(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			DirectoryInfo r = new DirectoryInfo(x);

			DirectoryInfo[] q = r.GetDirectories();

			if (!Directory.Exists(y))
			{
				Directory.CreateDirectory(y);
			}

			if (!Directory.Exists(y))
			{
				cd(x);
			}

			FileInfo[] f = r.GetFiles();

			foreach (FileInfo o in f)
			{
				string h = Path.Combine(y, o.Name);
				o.CopyTo(h, false);
			}

			foreach (DirectoryInfo u in q)
            {
                string l = Path.Combine(x, u.Name);

				dx(u.FullName, l);
            }

            pr((object)1);
		}

		internal static void dx(string x, string y)
		{
			DirectoryInfo r = new DirectoryInfo(x);

			DirectoryInfo[] q = r.GetDirectories();

			if (!Directory.Exists(y))
			{
				Directory.CreateDirectory(y);
			}

			if (!Directory.Exists(y))
			{
				cd(x);
			}

			FileInfo[] f = r.GetFiles();

			foreach (FileInfo o in f)
			{
				string h = Path.Combine(y, o.Name);
				o.CopyTo(h, false);
			}

			foreach (DirectoryInfo u in q)
			{
				string l = Path.Combine(x, u.Name);

				dx(u.FullName, l);
			}

            pr((object)1);
		}

		internal static void cs(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			var n = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).c;

			foreach (var z in s.Where(c => c.c == n))
			{
				z.v = x;
			}

			pr((object)y);
		}

		internal static void ci(string b)
		{
			var x = i.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = i.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			var n = i.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).c;

			foreach (var z in i.Where(c => c.c == n))
			{
				z.v = x;
			}

			pr((object)y);
		}

		internal static void cu(string b)
		{
			var x = d.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = d.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			var n = d.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).c;

			foreach (var z in d.Where(c => c.c == n))
			{
				z.v = x;
			}

			pr((object)y);
		}

		internal static void dt(string b)
		{
			var l = b.Substring(1, b.Length - 1);

			var z = DateTime.Now.ToString();

			s.Add(new s { c = l, v = z });

            pr((object)z);
		}

		internal static void wa(string b)
		{
			var x = i.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;

			Thread.Sleep(x);

            pr((object)1);
		}

		internal static void sp(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			TimeSpan t = DateTime.Parse(y).Subtract(DateTime.Parse(x));

			pr((object)t);
		}

		internal static void ex(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var z = s.Find(a => a.c == b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1)).v;

			var workingDirectory = x;
			var path = y;
			var arguments = z;

			ProcessStartInfo processStartInfo = new ProcessStartInfo();

			processStartInfo.WorkingDirectory = workingDirectory;
			processStartInfo.Arguments = arguments;
			processStartInfo.FileName = path;
			processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

			Process.Start(processStartInfo);

            pr((object)1);
		}

		internal static string sr(string b)
		{
			var x = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var z = s.Find(a => a.c == b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1)).v;
			var k = b.Split(',')[3].Substring(1, b.Split(',')[3].Length - 1);

			var h = x.Replace(y, z);

			s.Add(new s { c = k, v = h });

            pr((object)h);

			return h;
		}

		internal static void hs(string b)
		{
			var w = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var x = s.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var y = s.Find(a => a.c == b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1)).v;
			var z = s.Find(a => a.c == b.Split(',')[3].Substring(1, b.Split(',')[3].Length - 1)).v;
			var k = b.Split(',')[4].Substring(1, b.Split(',')[4].Length - 1);

			var q = WebRequest.Create(w) as HttpWebRequest;

			byte[] data = Encoding.ASCII.GetBytes(x);

			q.Method = y;
			q.ContentType = z;
			q.ContentLength = data.Length;

			if (!string.IsNullOrEmpty(x))
			{
				using (Stream m = q.GetRequestStream())
				{
					m.Write(data, 0, data.Length);
					m.Flush();
				}
			}

			string g = "";

			using (HttpWebResponse e = (HttpWebResponse)q.GetResponse())
			{
				using (Stream a = e.GetResponseStream())
				{
					using (StreamReader u = new StreamReader(a, Encoding.UTF8))
					{
						g = u.ReadToEnd();
					}
				}
			}

			s.Add(new s { c = k, v = g });

            pr((object)g);
		}

		internal static void fb(string b)
		{
			var w = s.Find(a => a.c == b.Split(',')[0].Substring(1, b.Split(',')[0].Length - 1)).v;
			var k = b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1);

			string e = "";

			using (BinaryReader c = new BinaryReader(File.Open(w, FileMode.Open)))
			{
				int o = 0;

				int g = (int)c.BaseStream.Length;

				while (o < g)
				{
					int v = c.ReadInt32();
					e = e + v;

					o += sizeof(int);
				}
			}

			s.Add(new s { c = k, v = e });

			pr((object)e);
		}

		internal static void bs(string b)
		{
			var r = b.Split(',');

			var o = "";

			string l = "";

			foreach (var t in r)
			{
				var h = t.Substring(0, 1);

				switch (h)
				{
					case "$":
						o = o + s.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "*":
						o = o + i.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case ".":
						o = o + d.Find(a => a.c == t.Substring(1, t.Length - 1)).v;
						break;
					case "'":
						o = o + vs(t.Substring(1, t.Length - 1));
						break;
					case "%":
						l = t.Substring(1, t.Length - 1);
						break;
				}
			}

			if (l != "")
			{
				s.Add(new s { c = l, v = o });
				pr((object)o);
			}
			else
				pr((object)o);
		}

		internal static void wh(string b)
		{
            var f = b.Split(',')[0];
            var x = i.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;
			var t = b.Split(',')[2].Substring(1, b.Split(',')[2].Length - 1);

			do
			{
				Second(new string[] { f.Replace("~", ">").Replace("-", "=") });
			}
			while (x == vi(t));
		}

		internal static void ds(string b)
		{
            var f = b.Split(',')[0];
            var x = i.Find(a => a.c == b.Split(',')[1].Substring(1, b.Split(',')[1].Length - 1)).v;

			var timer = new Timer(
				e => Second(new string[] { f.Replace("~", ">").Replace("-", "=") }),
				null,
				TimeSpan.Zero,
				TimeSpan.FromSeconds(x));
		}

		internal static void hl()
		{
			Console.WriteLine("---- DATATYPES ----");
			Console.WriteLine("$ - Defines a string. Example: $a=Hello World");
			Console.WriteLine("* - Defines a integer. Example: *a=1");
			Console.WriteLine(". - Defines a double. Example: .a=1.01");
			Console.WriteLine("---- FUNCTIONS ----");
			Console.WriteLine(">pr - Prints values. Example: >pr='Hello World - Variables: n");
			Console.WriteLine(">ad - Add integer and double. Example: >ad=*a,*b,.c,%d (opt out) - Variables: n");
			Console.WriteLine(">su - Subtract integer and double. Example: >su=*a,*b,.c,%d (opt out) - Variables: n");
			Console.WriteLine(">my - Multiply integer and double. Example: >my=*a,*b,%c (opt out) - Variables: 2");
			Console.WriteLine(">dv - Divide integer and double. Example: >dv=*a,*b,%c (opt out) - Variables: 2");
			Console.WriteLine(">cl - Extract a value inside a string. Example: >cl=$a,$b,$c,%d (out) - Variables: 4");
			Console.WriteLine(">dw - Download source data from a website. Example: >dw=$a - Variables: 1");
			Console.WriteLine(">df - Download file from a website. Example: >df=$a,$b - Variables: 2");
			Console.WriteLine(">fw - Write text to a file. Example: >fw=$a,$b - Variables: 2");
			Console.WriteLine(">fd - Delete a file. Example: >fw=$a - Variables: 1");
			Console.WriteLine(">fc - Copy a file. Example: >fc=$a - Variables: 2");
			Console.WriteLine(">cd - Create a directory. Example: >cd=$a - Variables: 1");
			Console.WriteLine(">dd - Delete a directory. Example: >dd=$a - Variables: 1");
			Console.WriteLine(">dc - Copy a directory. Example: >dc=$a,$b - Variables: 2");
			Console.WriteLine(">cs - Copy a value from one to another string. Example: >cs=$a,$b - Variables: 2");
			Console.WriteLine(">ci - Copy a value from one to another integer. Example: >ci=*a,*b - Variables: 2");
			Console.WriteLine(">cu - Copy a value from one to another double. Example: >ci=.a,.b - Variables: 2");
			Console.WriteLine(">dt - Print current date time. Example: >dt=%a (out) - Variables: 1");
			Console.WriteLine(">hl - Show help. Example: >hl=0 - Variables: 0");
			Console.WriteLine(">wa - Wait for a specific time. Example: >wa=*a - Variables: 1");
			Console.WriteLine(">sp - Get timespan. Example: >sp=$a,$b - Variables: 2");
			Console.WriteLine(">ex - Start a process. Example: >ex=$a,$b,$c - Variables: 3");
			Console.WriteLine(">sr - Replace value from string. Example: >sr=$a,$b,$c,%d (out) - Variables: 4");
			Console.WriteLine(">hs - Send a request to a url. Example: >hs=$a,$b,$c,$d,%e (out) - Variables: 5");
			Console.WriteLine(">fb - Read file binary. Example: >fb=$a,%b (out) - Variables: 2");
			Console.WriteLine(">bs - Build a string. Example: >bs='Hello World,%a (opt out) - Variables: n");
			Console.WriteLine("---- STATEMENTS ----");
			Console.WriteLine(">fr - For statement. Example: >fr=*a,$b,*c,~pr-§o - Variables: 4");
			Console.WriteLine(">wh - Do while statement. Example: >wh=~pr-$a,*b,'100 - Variables: 3");
			Console.WriteLine(">ds - Do statement. Example: >ds=~pr-$a,*b - Variables: 2");
			Console.WriteLine("---- FLAGS ----");
			Console.WriteLine("COMPILER FLAG - Return value. Usage: COMPILER.FLAG = true (Return: RETURN.VALUE)");
			Console.WriteLine("---- VARIABLES ----");
			Console.WriteLine("% - Output variable");
			Console.WriteLine("§o - Output from for function");
			Console.WriteLine("§p - Placeholder for -");
			Console.WriteLine("§q - Placeholder for =");
		}

		internal static void rl()
		{
			Console.ReadLine();
		}

		internal static string vs(string v)
		{
			return v.Replace("§p", ",").Replace("§q", "=");
		}

		internal static int vi(string v)
		{
			int t = 0;

			for (int x = 0; x < v.Length; x++)
			{
				char c = v[x];

				t = (t * 10) + (int)(c - '0'); 
			}

			return t;
		}

		internal static decimal vd(string v)
		{
			decimal[] w = { 1m, 10m, 100m, 1000m, 10000m, 100000m, 1000000m };

			var n = 0;

			int p = v.Length; 

			for (int k = 0; k < v.Length; k++)
			{ 
				char c = v[k]; 

				if (c == '.') 
					p = k + 1; 
				else 
					n = (n * 10) + (int)(c - '0'); 
			} 

			return n / w[v.Length - p]; 
		}

		static List<s> s;

		static List<i> i;

		static List<d> d;
	}

	public class s
	{
		public string c { get; set; }
		public string v { get; set; }
	}

	public class i
	{
		public string c { get; set; }
		public int v { get; set; }
	}

	public class d
	{
		public string c { get; set; }
		public decimal v { get; set; }
	}

	public class COMPILER
	{
		public static bool FLAG { get; set; }
	}

	public class RETURN
	{
		public static object VALUE { get; set; }
    }
}