## $ Introduction

What is x?

**x** is a simple **script language** written in C#. It can be used as a standalone executable or can be embedded in other programs. Because x uses only few standard namespaces, has no other third party dependencies and it's source code is stored in one single file it is very simple to embed.

x has approximately **1.300 lines of code**. The compiled script is about **40k**.

## $ Features

* Supported datatypes (string, integer, double)
* Statements (for, do while, do)
* Various functions (print, add, copy file)
* Able to return values

## $ Requirements

The only requirement is **.Net / Mono Framework**.

## $ Target platforms

* .NET Framework
* MONO (Windows, MacOS, Linux)
* Windows Phone 8
* WinRT
* Xamarin.Android
* Xamarin.iOS

## $ Build

sudo -s mcs x.cs

## $ Documentation

---- DATATYPES ----

```
$ - Defines a string. Example: $a=Hello World
* - Defines a integer. Example: *a=1
. - Defines a double. Example: .a=1.01
```

---- FUNCTIONS ----

```
>pr - Prints values. Example: >pr='Hello World - Variables: n
>ad - Add integer and double. Example: >ad=*a,*b,.c,%d (opt out) - Variables: n
>su - Subtract integer and double. Example: >su=*a,*b,.c,%d (opt out) - Variables: n
>my - Multiply integer and double. Example: >my=*a,*b,%c (opt out) - Variables: 2
>dv - Divide integer and double. Example: >dv=*a,*b,%c (opt out) - Variables: 2
>cl - Extract a value inside a string. Example: >cl=$a,$b,$c,%d (out) - Variables: 4
>dw - Download source data from a website. Example: >dw=$a - Variables: 1
>df - Download file from a website. Example: >df=$a,$b - Variables: 2
>fw - Write text to a file. Example: >fw=$a,$b - Variables: 2
>fd - Delete a file. Example: >fw=$a - Variables: 1
>fc - Copy a file. Example: >fc=$a - Variables: 2
>cd - Create a directory. Example: >cd=$a - Variables: 1
>dd - Delete a directory. Example: >dd=$a - Variables: 1
>dc - Copy a directory. Example: >dc=$a,$b - Variables: 2
>cs - Copy a value from one to another string. Example: >cs=$a,$b - Variables: 2
>ci - Copy a value from one to another integer. Example: >ci=*a,*b - Variables: 2
>cu - Copy a value from one to another double. Example: >ci=.a,.b - Variables: 2
>dt - Print current date time. Example: >dt=%a (out) - Variables: 1
>hl - Show help. Example: >hl=0 - Variables: 0
>wa - Wait for a specific time. Example: >wa=*a - Variables: 1
>sp - Get timespan. Example: >sp=$a,$b - Variables: 2
>ex - Start a process. Example: >ex=$a,$b,$c - Variables: 3
>sr - Replace value from string. Example: >sr=$a,$b,$c,%d (out) - Variables: 4
>hs - Send a request to a url. Example: >hs=$a,$b,$c,$d,%e (out) - Variables: 5
>fb - Read file binary. Example: >fb=$a,%b (out) - Variables: 2
>bs - Build a string. Example: >bs='Hello World,%a (opt out) - Variables: n
```

---- STATEMENTS ----

```
>fr - For statement. Example: >fr=*a,$b,*c,~pr-§o - Variables: 4
>wh - Do while statement. Example: >wh=~pr-$a,*b,'100 - Variables: 3
>ds - Do statement. Example: >ds=~pr-$a,*b - Variables: 2
```

---- FLAGS ----

```
COMPILER FLAG - Return value. Usage: COMPILER.FLAG = true (Return: RETURN.VALUE)
```

---- VARIABLES ----

```
% - Output variable
§o - Output from for function
§p - Placeholder for -
§q - Placeholder for =
```

## $ Sample usage

mono x.exe script.x

## $ Sample script

```
$a=Hello World
>pr=$a
```
